import React, {PropTypes} from 'react'
import {Tabs} from 'antd'
const TabPane = Tabs.TabPane

const Tab = ({ tabStatusChange }) => (
  <Tabs defaultActiveKey="0" onChange={tabStatusChange}>
    <TabPane tab="全部" key="0"></TabPane>
    <TabPane tab="年审待提交" key="1"></TabPane>
    <TabPane tab="年审待审核" key="2"></TabPane>
    <TabPane tab="年审拒绝待关闭" key="5"></TabPane>
  </Tabs>
)

Tab.propTypes = {
  tabStatusChange: PropTypes.func.isRequired
}

export default Tab
