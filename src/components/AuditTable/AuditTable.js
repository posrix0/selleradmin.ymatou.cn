import React, {PropTypes} from 'react'
import {Table, message} from 'antd'
import isEmpty from 'lodash/isEmpty'
import Tab from './Tab'
import {ROOT} from '../../resource'
import queryString from 'query-string'
import './AuditTable.module.css'

class AuditTable extends React.Component {
  static propTypes = {
    collection: PropTypes.array.isRequired,
    query: PropTypes.object.isRequired,
    total: PropTypes.number.isRequired,
    pageChange: PropTypes.func.isRequired,
    tabStatusChange: PropTypes.func.isRequired,
    toggleAuditModalVisible: PropTypes.func.isRequired,
    getAuditDetail: PropTypes.func.isRequired
  }

  showModal = (auditId) => {
    const {toggleAuditModalVisible, getAuditDetail} = this.props
    getAuditDetail(auditId, (response) => {
      const {AnnualReview} = response
      isEmpty(AnnualReview) ? message.info('无详情数据') : toggleAuditModalVisible(true)
    })
  }

  render() {
    const columns = [
      {
        title: '最新提交时间',
        dataIndex: 'date'
      }, {
        title: '用户名',
        dataIndex: 'username'
      }, {
        title: '国家',
        dataIndex: 'country'
      }, {
        title: '邮箱',
        dataIndex: 'email'
      }, {
        title: '状态',
        dataIndex: 'status'
      }, {
        title: '买手类型',
        dataIndex: 'type'
      }, {
        title: '审核操作',
        dataIndex: 'operation',
        render: (text, record) => {
          const {key} = record
          return <a onClick={this.showModal.bind(this, key)}>{text}</a>
        }
      }
    ]

    const {collection, query, total, pageChange, tabStatusChange} = this.props

    const pagination = {
      total: total,
      current: query.PageIndex,
      onChange: current => {
        pageChange(current)
      }
    }

    return (
      <div>
        <Tab tabStatusChange={tabStatusChange}/>
        <Table
          columns={columns}
          dataSource={collection}
          bordered
          footer={() => {
            return <a href={`${ROOT}AnnualReview/AnnualReviewMange/ExportAnnualReviewList?${queryString.stringify(query)}`}>导出当前列表结果</a>
          }}
          pagination={pagination}/>
      </div>
    )
  }
}

export default AuditTable
