import React, {PropTypes} from 'react'
import {Popover, Icon, Button, message} from 'antd'
import AnnualReviewMangeResource from '../../resource/AnnualReviewMangeResource'
import styles from './AuditModal.module.css'

const oldValue = (title, content, comparison) => {
  return (content === comparison) ? false : <Popover
      placement="bottomLeft" title={title}
      content={content} trigger="hover">
      <Icon className={styles.oldValueIcon} type="exclamation-circle"/>
    </Popover>
}

class Picture extends React.Component {
  static propTypes = {
    annualReview: PropTypes.object,
    annualReviewUserInfo: PropTypes.object,
    annualReviewCompanyInfo: PropTypes.object,
    annualReviewLogList: PropTypes.array
  }

  static contextTypes = {
    isCompanySeller: PropTypes.bool
  }

  constructor(props) {
    super(props)

    this.state = {
      feedbackNodeList: null
    }
  }

  handleDownloadProxyLetter(address) {
    address ? window.open(address) : message.info('未上传授权委托书')
  }

  getFeedbacksByLogId(logId) {
    AnnualReviewMangeResource.getFeedbacksByLogId(logId)
      .then(response => {
        const feedbacks = response.data.Data,
          feedbackNodeList = feedbacks.length === 0 ? <div>无更多记录</div> : (
              <ul>
                {
                  feedbacks.map((feedback, index) => {
                    return <li key={index}>
                      {`${index + 1}. ${feedback.Value}`}
                    </li>
                  })
                }
              </ul>
            )

        this.setState({
          feedbackNodeList
        })
      })
  }

  render() {
    const {annualReview, annualReviewUserInfo, annualReviewCompanyInfo, annualReviewLogList} = this.props

    return !this.context.isCompanySeller ? (
        <div className={styles.detail}>
          <ul>
            <li>
              <span>申请时间：{annualReview.AddTime}</span>
              <span>
                {
                  oldValue('曾用用户名',
                    annualReviewUserInfo.OldLoginId,
                    annualReview.SellerLoginName)
                }
                用户名：{annualReview.SellerLoginName}
              </span>
            </li>
            <li>
              <hr/>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用姓',
                    annualReviewUserInfo.OldLastName,
                    annualReviewUserInfo.LastName)
                }
                姓：{annualReviewUserInfo.LastName}
              </span>
              <span>
                {
                  oldValue('曾用名',
                    annualReviewUserInfo.OldFirstName,
                    annualReviewUserInfo.FirstName)
                }
                名：{annualReviewUserInfo.FirstName}
              </span>
            </li>
            <li>
              <span>手机号：{annualReviewUserInfo.Phone}</span>
              <span>邮箱：{annualReviewUserInfo.Email}</span>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用身份证明有效时间',
                    `${annualReviewUserInfo.OldOverseaIdentity_EffectiveTime}----${annualReviewUserInfo.OldOverseaIdentity_OverdueTime}`,
                    `${annualReviewUserInfo.OverseaIdentity_EffectiveTime}----${annualReviewUserInfo.OverseaIdentity_OverdueTime}`)
                }
                身份证明有效时间：{annualReviewUserInfo.OverseaIdentity_EffectiveTime}----{annualReviewUserInfo.OverseaIdentity_OverdueTime}
              </span>
            </li>
            <li>
              <span>身份证明：{annualReviewUserInfo.OverseaIdentityTypeName}</span>
              <span>居住证明：{annualReviewUserInfo.OverseaResidenceProveTypeName}</span>
            </li>
            <li>
              <hr/>
            </li>
            <li>
              <span>大区：{annualReview.ContinentName}</span>
              <span>国家：{annualReviewUserInfo.CountryName}</span>
              <span>州/省，城市：{annualReviewUserInfo.StateName},{annualReviewUserInfo.CityName}</span>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用地址',
                    annualReviewUserInfo.OldAddress,
                    annualReviewUserInfo.Address)
                }
                详细地址：{annualReviewUserInfo.Address}
              </span>
            </li>
            <li>
              <span>邮编：{annualReviewUserInfo.PostCode}</span>
            </li>
            <li>
              <hr/>
            </li>
            <li>
              <span>该账号审核记录</span>
            </li>
            {
              annualReviewLogList.map((annualReviewLog, index) => {
                const title = annualReviewLog.LogType === 8 ? '撤销拒绝原因' : '拒绝原因'
                return (
                  <li key={index}>
                  <span>
                    {annualReviewLog.AddTime} / {annualReviewLog.LogContent}
                    {
                      (annualReviewLog.LogType === 8 || annualReviewLog.LogType === 7 || annualReviewLog.LogType === 6)
                      && <Popover placement="bottomLeft" title={title} content={this.state.feedbackNodeList}
                                  trigger="click">
                        <Button size="small" className={styles.more}
                                onClick={this.getFeedbacksByLogId.bind(this, annualReviewLog.AnnualReviewLogId)}>{title}</Button>
                      </Popover>
                    }
                  </span>
                  </li>
                )
              })
            }
          </ul>
        </div>
      ) : (
        <div className={styles.detail}>
          <ul>
            <li>
              <span>申请时间：{annualReview.AddTime}</span>
              <span>
                {
                  oldValue('曾用用户名',
                    annualReviewUserInfo.OldLoginId,
                    annualReview.SellerLoginName)
                }
                用户名：{annualReview.SellerLoginName}
              </span>
            </li>
            <li>
              <hr/>
            </li>
            <li>
              <span>国家：{annualReview.CountryName}</span>
              <span>邮箱：{annualReviewUserInfo.Email}</span>
            </li>
            <li>
              <span>州/省，城市：{annualReviewUserInfo.StateName},{annualReviewUserInfo.CityName}</span>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用公司名称',
                    annualReviewCompanyInfo.OldCompanyName,
                    annualReviewCompanyInfo.CompanyName)
                }
                公司名称：{annualReviewCompanyInfo.CompanyName}
              </span>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用法定代表人姓：',
                    annualReviewCompanyInfo.OldLegalPersonLastName,
                    annualReviewCompanyInfo.LegalPersonLastName)
                }
                法定代表人姓：{annualReviewCompanyInfo.LegalPersonLastName}
              </span>
              <span>
                {
                  oldValue('曾用名',
                    annualReviewCompanyInfo.OldLegalPersonFirstName,
                    annualReviewCompanyInfo.LegalPersonFirstName)
                }
                名：{annualReviewCompanyInfo.LegalPersonFirstName}
              </span>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用营业执照编号',
                    annualReviewCompanyInfo.OldLicenseNumber,
                    annualReviewCompanyInfo.LicenseNumber)
                }
                营业执照编号：{annualReviewCompanyInfo.LicenseNumber}
              </span>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用营业执照有效时间',
                    `${annualReviewCompanyInfo.OldLicense_EffectiveTime}----${annualReviewCompanyInfo.OldLicense_OverdueTime}`,
                    `${annualReviewCompanyInfo.License_EffectiveTime}----${annualReviewCompanyInfo.License_OverdueTime}`)
                }
                营业执照有效时间：{annualReviewCompanyInfo.License_EffectiveTime}----{annualReviewCompanyInfo.License_OverdueTime}
              </span>
            </li>
            <li>
              <span>
                {
                  oldValue('曾用注册地址：',
                    annualReviewCompanyInfo.OldCompanyAddress,
                    annualReviewCompanyInfo.CompanyAddress)
                }
                注册地址：{annualReviewCompanyInfo.CompanyAddress}
              </span>
            </li>
            <li>
              <hr/>
            </li>
            <li>
              <span>被授权人为法人：{annualReviewCompanyInfo.IsAuthorizeProxy ? '是' : '否'}</span>
              {
                !annualReviewCompanyInfo.IsAuthorizeProxy && (
                  <span>
                  授权委托书：<a onClick={this.handleDownloadProxyLetter.bind(this, annualReviewCompanyInfo.ProxyLetter)}>点击下载</a>
                  </span>
                )
              }
            </li>
            <li>
              <span>
                {
                  oldValue('曾用被授权人姓：',
                  annualReviewCompanyInfo.OldProxyManLastName,
                  annualReviewCompanyInfo.ProxyManLastName)
                }
                被授权人姓：{annualReviewCompanyInfo.ProxyManLastName}
              </span>
              <span>
                {
                  oldValue('曾用名',
                  annualReviewCompanyInfo.OldProxyManFirstName,
                  annualReviewCompanyInfo.ProxyManFirstName)
                }
                名：{annualReviewCompanyInfo.ProxyManFirstName}
              </span>
            </li>
            <li>
              <span>手机号：{annualReviewUserInfo.Phone}</span>
              <span>
                {
                  oldValue('曾用身份证明有效时间',
                  `${annualReviewUserInfo.OldOverseaIdentity_EffectiveTime}----${annualReviewUserInfo.OldOverseaIdentity_OverdueTime}`,
                  `${annualReviewUserInfo.OverseaIdentity_EffectiveTime}----${annualReviewUserInfo.OverseaIdentity_OverdueTime}`)
                }
                身份证明有效时间：{annualReviewUserInfo.OverseaIdentity_EffectiveTime}----{annualReviewUserInfo.OverseaIdentity_OverdueTime}
              </span>
            </li>
            <li>
              <span>身份证明：{annualReviewUserInfo.OverseaIdentityTypeName}</span>
              <span>居住证明：{annualReviewUserInfo.OverseaResidenceProveTypeName}</span>
            </li>
            <li>
              <hr/>
            </li>
            <li>
              <span>该账号审核记录</span>
            </li>
            {
              annualReviewLogList.map((annualReviewLog, index) => {
                const title = annualReviewLog.LogType === 8 ? '撤销拒绝原因' : '拒绝原因'
                return (
                  <li key={index}>
                  <span>
                    {annualReviewLog.AddTime} / {annualReviewLog.LogContent}
                    {
                      (annualReviewLog.LogType === 8 || annualReviewLog.LogType === 7 || annualReviewLog.LogType === 6)
                      && <Popover placement="bottomLeft" title={title} content={this.state.feedbackNodeList}
                                  trigger="click">
                        <Button size="small" className={styles.more}
                                onClick={this.getFeedbacksByLogId.bind(this, annualReviewLog.AnnualReviewLogId)}>{title}</Button>
                      </Popover>
                    }
                  </span>
                  </li>
                )
              })
            }
          </ul>
        </div>
      )
  }
}

export default Picture
