import React, {PropTypes} from 'react'
import styles from './AuditModal.module.css'
import {Button, Input} from 'antd'
import update from 'immutability-helper'

class RejectRevoke extends React.Component {
  static PropTypes = {
    showRejectRevoke: PropTypes.bool.isRequired,
    handleToggleRejectRevoke: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      value: ''
    }
  }

  handleRejectRevokeChange = (e) => {
    const {value} = e.target

    this.setState(
      update(this.state, {
        value: {$set: value}
      })
    )
  }

  render() {
    const {showRejectRevoke, rejectRevokeCancelHandle, rejectRevokeSubmitHandle} = this.props

    return <div style={{display: showRejectRevoke ? 'block' : 'none'}}>
      <div className={styles.wrapper}>
        <div className={styles.reasonTitle}>
          <Input
            placeholder="撤销原因"
            className={styles.reasonInput}
            value={this.state.value}
            onChange={this.handleRejectRevokeChange}/>
        </div>
      </div>
      <div className={styles.reasonBtn}>
        <Button
          type="default"
          size="large"
          onClick={rejectRevokeCancelHandle}>
          取消
        </Button>
        <Button
          type="primary"
          size="large"
          onClick={rejectRevokeSubmitHandle.bind(this, this.state.value)}>
          撤销
        </Button>
      </div>
    </div>
  }
}

export default RejectRevoke
