import React, {PropTypes} from 'react'
import styles from './AuditModal.module.css'
import Title from './Title.js'
import Footer from './Footer.js'
import Picture from './Picture.js'
import Detail from './Detail.js'
import {Modal} from 'antd'

class AuditModal extends React.Component {
  static propTypes = {
    auditModalVisible: PropTypes.bool.isRequired,
    auditModalRandomKey: PropTypes.number.isRequired,
    toggleAuditModalVisible: PropTypes.func.isRequired,
    generateAuditModalRandomKey: PropTypes.func.isRequired,
    modifyAuditStatus: PropTypes.func.isRequired,
    annualReview: PropTypes.object.isRequired,
    annualReviewUserInfo: PropTypes.object.isRequired,
    annualReviewCompanyInfo: PropTypes.object.isRequired,
    annualReviewLogList: PropTypes.array.isRequired
  }

  static childContextTypes = {
    status: PropTypes.number,
    isCompanySeller: PropTypes.bool,
    id: PropTypes.string
  }

  getChildContext() {
    const {annualReview} = this.props

    return {
      status: annualReview.Status,
      isCompanySeller: annualReview.IsCompanySeller,
      id: annualReview.AnnualReviewId
    }
  }

  handleCancelModal = () => {
    const {toggleAuditModalVisible, generateAuditModalRandomKey} = this.props
    toggleAuditModalVisible(false)
    generateAuditModalRandomKey()
  }

  render() {
    const {auditModalVisible, modifyAuditStatus, auditModalRandomKey, annualReview, annualReviewUserInfo, annualReviewCompanyInfo, annualReviewLogList} = this.props,
      placehold = 'http://placehold.it/700x700',
      {
        PersonalPhotograph,
        OverseaIdentity_FrontPicture,
        OverseaIdentity_BackPicture,
        OverseaResidenceProvePicture,
        OldPersonalPhotograph,
        OldOverseaIdentity_FrontPicture,
        OldOverseaIdentity_BackPicture,
        OldOverseaResidenceProvePicture
      } = annualReviewUserInfo

    let picture = {
      personal: {
        name: '个人近照',
        src: PersonalPhotograph || placehold,
        oldValue: OldPersonalPhotograph,
        index: 0
      },
      identityFront: {
        name: '身份证明正面',
        src: OverseaIdentity_FrontPicture || placehold,
        oldValue: OldOverseaIdentity_FrontPicture,
        index: 1
      },
      identityBack: {
        name: '身份证明反面',
        src: OverseaIdentity_BackPicture || placehold,
        oldValue: OldOverseaIdentity_BackPicture,
        index: 2
      },
      residenceProve: {
        name: '居住证明',
        src: OverseaResidenceProvePicture || placehold,
        oldValue: OldOverseaResidenceProvePicture,
        index: 3
      }
    }

    if (annualReview.IsCompanySeller) {
      const {
        OrtherStatement_A,
        OrtherStatement_B,
        OrtherStatement_C,
        CompanyLicense,
        LegalPersonIdentityPicture,
        BankAccountStatement,
        OldOrtherStatement_A,
        OldOrtherStatement_B,
        OldOrtherStatement_C,
        OldCompanyLicense,
        OldLegalPersonIdentityPicture,
        OldBankAccountStatement,
      } = annualReviewCompanyInfo

      Object.assign(picture, {
        proxyLetter: {
          name: '营业执照',
          src: CompanyLicense || placehold,
          oldValue: OldCompanyLicense,
          index: 4
        },
        legalPersonIdentityPicture: {
          name: '法人身份证照片',
          src: LegalPersonIdentityPicture || placehold,
          oldValue: OldLegalPersonIdentityPicture,
          index: 5
        },
        bankAccountStatement: {
          name: '公司银行对账单',
          src: BankAccountStatement || placehold,
          oldValue: OldBankAccountStatement,
          index: 6
        },
        ortherStatement_A: {
          name: '其他资质',
          src: OrtherStatement_A || placehold,
          oldValue: OldOrtherStatement_A,
          index: 7
        },
        ortherStatement_B: {
          name: '其他资质',
          src: OrtherStatement_B || placehold,
          oldValue: OldOrtherStatement_B,
          index: 8
        },
        ortherStatement_C: {
          name: '其他资质',
          src: OrtherStatement_C || placehold,
          oldValue: OldOrtherStatement_C,
          index: 9
        }
      })
    }

    return (
      <Modal
        title={
          <Title />
        }
        visible={auditModalVisible}
        key={auditModalRandomKey}
        onCancel={this.handleCancelModal}
        width={900}
        footer={
          <Footer handleConfirmSubmit={this.handleConfirmSubmit} cancelModalHandle={this.handleCancelModal} modifyAuditStatus={modifyAuditStatus}/>
        }>
        <div className={styles.wrapper}>
          <Picture picture={picture}/>
          <Detail annualReview={annualReview} annualReviewCompanyInfo={annualReviewCompanyInfo}
                  annualReviewUserInfo={annualReviewUserInfo} annualReviewLogList={annualReviewLogList}/>
        </div>
      </Modal>
    )
  }
}

export default AuditModal
