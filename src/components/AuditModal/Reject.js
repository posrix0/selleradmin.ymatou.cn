import React, {PropTypes} from 'react'
import styles from './AuditModal.module.css'
import {Input, Checkbox, Button} from 'antd'
import update from 'immutability-helper'
import isEmpty from 'lodash/isEmpty'

class Reject extends React.Component {
  static PropTypes = {
    showReject: PropTypes.bool.isRequired,
    rejectTypes: PropTypes.array.isRequired,
    rejectCancelHandle: PropTypes.func.isRequired,
    rejectSubmitHandle:PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    const initState = {
      lock: false,
      query: [],
      reasons: []
    }

    this.state = {
      ...initState
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.rejectTypes !== nextProps.rejectTypes) {
      const rejectTypes = nextProps.rejectTypes.map(type => {
        return {
          key: type.Key,
          name: type.Value,
          details: type.Remarks,
          checked: false,
          value: '',
          showInput: false,
          showDetail: false
        }
      })

      this.setState(
        update(this.state, {
          reasons: {$set: rejectTypes}
        })
      )
    }
  }

  concealInputs = () => {
    return this.state.reasons.map(reason => {
      return {
        ...reason,
        showInput: false
      }
    })
  }

  restoreInputs = () => {
    return this.state.reasons.map(reason => {
      return reason.checked ? {
          ...reason,
          showInput: true
        } : {
          ...reason
        }
    })
  }

  isTargetChecked = (key) => {
    for (const reason of this.state.reasons) {
      if (reason.key === key) {
        return reason.checked
      }
    }
  }

  handleCheckType = (key) => {
    if (!this.isTargetChecked(key)) {
      const reasons = this.concealInputs().map(reason => {
        return reason.key === key ? {
            ...reason,
            checked: true,
            showDetail: true
          } : {
            ...reason
          }
      })

      this.setState(
        update(this.state, {
          reasons: {$set: reasons},
          lock: {$set: true}
        })
      )
    } else {
      const reasons = this.state.reasons.map(reason => {
        return reason.key === key ? {
            ...reason,
            checked: false,
            showInput: false,
            value: ''
          } : {
            ...reason
          }
      })

      this.setState(
        update(this.state, {
          reasons: {$set: reasons},
          lock: {$set: false}
        })
      )
    }
  }

  handleChangeDetail = (key, e) => {
    const {innerText} = e.target,
      reasons = this.restoreInputs().map(reason => {
        return reason.key === key ? {
            ...reason,
            value: innerText,
            showDetail: false,
            showInput: true
          } : {
            ...reason
          }
      })

    this.setState(
      update(this.state, {
        reasons: {$set: reasons},
        lock: {$set: false}
      })
    )
  }

  handleChangeValue = (key, e) => {
    const {value} = e.target,
      reasons = this.state.reasons.map(reason => {
        return reason.key === key ? {
            ...reason,
            value: value,
          } : {
            ...reason
          }
      })

    this.setState(
      update(this.state, {
        reasons: {$set: reasons}
      })
    )
  }

  handleSubmitReject = status => {
    let rejectList = []

    for (const reason of this.state.reasons) {
      if (reason.checked && !isEmpty(reason.value)) {
        rejectList.push({
          Key: reason.key,
          Value: reason.value
        })
      }
    }

    const {rejectSubmitHandle} = this.props

    rejectSubmitHandle(rejectList)
  }

  render() {
    const {showReject, rejectText, rejectCancelHandle} = this.props

    return this.state.reasons.length === 0 ? false : <div style={{display: showReject ? 'block' : 'none'}}>
        <div className={styles.wrapper}>
          <div
            style={{width: this.state.lock ? '40%' : 'auto'}}
            className={styles.reasonTitle}>
            <ul>
              {
                this.state.reasons.map((reason, index) => {
                  return <li key={index}>
                    <Checkbox
                      onChange={this.handleCheckType.bind(this, reason.key)}
                      disabled={this.state.lock}>
                      {reason.name}
                    </Checkbox>
                    <Input
                      style={{display: reason.showInput ? 'inline-block' : 'none'}}
                      className={styles.reasonInput}
                      value={reason.value}
                      onChange={this.handleChangeValue.bind(this, reason.key)}/>
                  </li>
                })
              }
            </ul>
          </div>
          {
            this.state.reasons.map((reason, reasonIndex) => {
              return <div
                style={{display: reason.showDetail ? 'block' : 'none'}}
                className={styles.reasonDetail}
                key={reasonIndex}>
                <ul>
                  {
                    reason.details.map((detail, detailIndex) => {
                      return <li
                        key={detailIndex}
                        onClick={this.handleChangeDetail.bind(this, reason.key)}>
                        {detail}
                      </li>
                    })
                  }
                </ul>
              </div>
            })
          }
        </div>
        <div className={styles.reasonBtn}>
          <Button
            type="default"
            size="large"
            onClick={rejectCancelHandle}>
            取消
          </Button>
          <Button
            type="primary"
            size="large"
            onClick={this.handleSubmitReject}>
            {rejectText}
          </Button>
        </div>
      </div>
  }
}

export default Reject
