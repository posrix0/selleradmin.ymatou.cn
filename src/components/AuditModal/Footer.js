import React, {PropTypes} from 'react'
import styles from './AuditModal.module.css'
import {Popconfirm, message, Button} from 'antd'
import update from 'immutability-helper'
import Reject from './Reject'
import RejectRevoke from './RejectRevoke'
import AnnualReviewResource from '../../resource/AnnualReviewResource'
import STATUS from '../../constants/Status'
import AnnualReviewMangeResource from '../../resource/AnnualReviewMangeResource'

class Footer extends React.Component {
  static contextTypes = {
    status: PropTypes.number,
    id: PropTypes.string,
    isCompanySeller: PropTypes.bool
  }

  static propTypes = {
    toggleAuditModalVisible: PropTypes.func,
    cancelModalHandle: PropTypes.func,
    modifyAuditStatus: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {
      showReject: false,
      showRejectForever: false,
      showRejectRevoke: false,
      rejectTypes: []
    }
  }

  componentWillMount() {
    AnnualReviewMangeResource.getRejectReasonTypeList(this.context.isCompanySeller)
      .then(response => {
        const {Data} = response.data

        this.setState(
          update(this.state, {
            rejectTypes: {$set: Data}
          })
        )
      })
  }

  handleSubmitApprove = () => {
    const {id} = this.context

    AnnualReviewResource.auditApprove({
      ID: id
    }).then(response => {
      const {modifyAuditStatus, cancelModalHandle} = this.props

      message.success('操作成功')
      modifyAuditStatus(STATUS.APPROVE.TEXT, id)
      cancelModalHandle()
    })
  }

  handleSubmitReject = (rejectList) => {
    const {id} = this.context

    AnnualReviewResource.auditReject({
      ID: id,
      RejectList: rejectList
    }).then(response => {
      const {modifyAuditStatus, cancelModalHandle} = this.props

      message.success('操作成功')
      modifyAuditStatus(STATUS.REJECT.TEXT, id)
      cancelModalHandle()
    })
  }

  handleSubmitRejectForever = (rejectList) => {
    const {id} = this.context

    AnnualReviewResource.auditRejectForever({
      ID: id,
      RejectList: rejectList
    }).then(response => {
      const {modifyAuditStatus, cancelModalHandle} = this.props

      message.success('操作成功')
      modifyAuditStatus(STATUS.REJECT_FOREVER.TEXT, id)
      cancelModalHandle()
    })
  }

  handleSubmitRejectRevoke = (revokeText) => {
    const {id} = this.context

    AnnualReviewResource.auditRejectRevoke({
      ID: id,
      Remark: revokeText
    }).then(response => {
      const {modifyAuditStatus, cancelModalHandle} = this.props

      message.success('操作成功')
      modifyAuditStatus(STATUS.WAITING_POST.TEXT, id)
      cancelModalHandle()
    })
  }

  handleToggleReject = () => {
    this.setState(
      update(this.state, {
        showReject: {$set: !this.state.showReject}
      })
    )
  }

  handleToggleRejectForever = () => {
    this.setState(
      update(this.state, {
        showRejectForever: {$set: !this.state.showRejectForever}
      })
    )
  }

  handleToggleRejectRevoke = () => {
    this.setState(
      update(this.state, {
        showRejectRevoke: {$set: !this.state.showRejectRevoke}
      })
    )
  }

  render() {
    const {status} = this.context

    switch (status) {
      case STATUS.WAITING_AUDIT.ID:
        return <footer className={styles.footer}>
          <div style={{
            display: (this.state.showReject || this.state.showRejectForever) ? 'none' : 'block'
          }}>
            <Popconfirm title="确定让该用户通过年审吗？该操作无法撤销" onConfirm={this.handleSubmitApprove} okText="确定" cancelText="取消">
              <Button type="primary" size="large">年审通过</Button>
            </Popconfirm>
            <Button type="default" size="large" onClick={this.handleToggleReject}>年审退回</Button>
            <Button type="default" size="large" onClick={this.handleToggleRejectForever}>彻底拒绝</Button>
          </div>
          <Reject
            showReject={this.state.showReject}
            rejectTypes={this.state.rejectTypes}
            rejectText="年审退回"
            rejectCancelHandle={this.handleToggleReject}
            rejectSubmitHandle={this.handleSubmitReject}/>
          <Reject
            showReject={this.state.showRejectForever}
            rejectTypes={this.state.rejectTypes}
            rejectText="彻底拒绝"
            rejectCancelHandle={this.handleToggleRejectForever}
            rejectSubmitHandle={this.handleSubmitRejectForever}/>
        </footer>
      case STATUS.WAITING_POST.ID:
      case STATUS.REJECT.ID:
        return <footer className={styles.footer}>
          <div style={{
            display: this.state.showRejectForever ? 'none' : 'block'
          }}>
            <Button type="default" size="large" onClick={this.handleToggleRejectForever}>彻底拒绝</Button>
          </div>
          <Reject
            showReject={this.state.showRejectForever}
            rejectTypes={this.state.rejectTypes}
            rejectText="彻底拒绝"
            rejectCancelHandle={this.handleToggleRejectForever}
            rejectSubmitHandle={this.handleSubmitRejectForever}/>
        </footer>
      case STATUS.REJECT_FOREVER.ID:
        return <footer className={styles.footer}>
          <div style={{
            display: this.state.showRejectRevoke ? 'none' : 'block'
          }}>
            <Button type="default" size="large" onClick={this.handleToggleRejectRevoke}>撤销拒绝</Button>
          </div>
          <RejectRevoke showRejectRevoke={this.state.showRejectRevoke}
                        rejectRevokeCancelHandle={this.handleToggleRejectRevoke}
                        rejectRevokeSubmitHandle={this.handleSubmitRejectRevoke}/>
        </footer>
      case STATUS.APPROVE.ID:
      default:
        return false
    }
  }
}

export default Footer
