import React, {PropTypes} from 'react'
import styles from './AuditModal.module.css'
import {Row, Col, Icon} from 'antd'
import Carousel from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import pdf from '../../assets/pdf.jpg'
import doc from '../../assets/doc.jpg'

const getExt = filename => {
  const index = filename.lastIndexOf(".") + 1,
  ext = filename.substr(index)

  return ext
}

class Picture extends React.Component {
  static propTypes = {
    picture: PropTypes.object.isRequired
  }

  static contextTypes = {
    status: PropTypes.number,
    isCompanySeller: PropTypes.bool
  }

  constructor(props) {
    super(props)
    this.state = {
      showCarousel: false
    }
    this.handleCloseCarousel = this.handleCloseCarousel.bind(this)
  }

  presrc(filename) {
    const ext = getExt(filename)

    switch (ext) {
      case 'pdf':
        return pdf
      case 'doc':
        return doc
      default:
        return filename
    }
  }

  handleOpenFile(index, filename) {
    const ext = getExt(filename)

    switch (ext) {
      case 'pdf':
      case 'doc':
        window.open(filename)
        break
      default:
        this.handleOpenCarousel(index)
    }
  }

  handleOpenCarousel(index) {
    this.setState({
      showCarousel: true
    }, () => {
      this.carousel.slickGoTo(index)
    })
  }

  handleCloseCarousel() {
    this.setState({
      showCarousel: false
    })
  }

  render() {
    const {picture} = this.props

    const settings = {
      customPaging: i => {
        for (let value of Object.values(picture)) {
          if (i === value.index) {
            return <a><img alt={value.name} src={this.presrc(value.src)}/></a>
          }
        }
      },
      dots: true,
      dotsClass: 'slick-dots',
      infinite: true,
      swipeToSlide: false,
      speed: 500,
      useCSS: false,
      slidesToShow: 1,
      slidesToScroll: 1
    }

    return this.context.isCompanySeller ? (<div>
          <div className={styles.picture}>
            <Row type="flex" justify="space-around" align="middle" className={styles.pictureRow}>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.personal.name} src={this.presrc(picture.personal.src)}
                     onClick={this.handleOpenFile.bind(this, picture.personal.index, picture.personal.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.personal.oldValue && <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.personal.name}
                </h4>
              </Col>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.identityFront.name} src={this.presrc(picture.identityFront.src)}
                     onClick={this.handleOpenFile.bind(this, picture.identityFront.index, picture.identityFront.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.identityFront.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.identityFront.name}
                </h4>
              </Col>
            </Row>
            <Row type="flex" justify="space-around" align="middle" className={styles.pictureRow}>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.identityBack.name} src={this.presrc(picture.identityBack.src)}
                     onClick={this.handleOpenFile.bind(this, picture.identityBack.index, picture.identityBack.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.identityBack.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.identityBack.name}
                </h4>
              </Col>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.residenceProve.name} src={this.presrc(picture.residenceProve.src)}
                     onClick={this.handleOpenFile.bind(this, picture.residenceProve.index, picture.residenceProve.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.residenceProve.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.residenceProve.name}
                </h4>
              </Col>
            </Row>
            <Row type="flex" justify="space-around" align="middle" className={styles.pictureRow}>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.bankAccountStatement.name} src={this.presrc(picture.bankAccountStatement.src)}
                     onClick={this.handleOpenFile.bind(this, picture.bankAccountStatement.index, picture.bankAccountStatement.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.bankAccountStatement.oldValue && <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.bankAccountStatement.name}
                </h4>
              </Col>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.proxyLetter.name} src={this.presrc(picture.proxyLetter.src)}
                     onClick={this.handleOpenFile.bind(this, picture.proxyLetter.index, picture.proxyLetter.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.proxyLetter.oldValue && <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.proxyLetter.name}
                </h4>
              </Col>
            </Row>
            <Row type="flex" justify="space-around" align="middle" className={styles.pictureRow}>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.legalPersonIdentityPicture.name}
                     src={this.presrc(picture.legalPersonIdentityPicture.src)}
                     onClick={this.handleOpenFile.bind(this, picture.legalPersonIdentityPicture.index, picture.legalPersonIdentityPicture.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.legalPersonIdentityPicture.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.legalPersonIdentityPicture.name}
                </h4>
              </Col>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.ortherStatement_A.name}
                     src={this.presrc(picture.ortherStatement_A.src)}
                     onClick={this.handleOpenFile.bind(this, picture.ortherStatement_A.index, picture.ortherStatement_A.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.ortherStatement_A.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.ortherStatement_A.name}
                </h4>
              </Col>
            </Row>
            <Row type="flex" justify="space-around" align="middle" className={styles.pictureRow}>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.ortherStatement_B.name}
                     src={this.presrc(picture.ortherStatement_B.src)}
                     onClick={this.handleOpenFile.bind(this, picture.ortherStatement_B.index, picture.ortherStatement_B.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.ortherStatement_B.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.ortherStatement_B.name}
                </h4>
              </Col>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.ortherStatement_C.name}
                     src={this.presrc(picture.ortherStatement_C.src)}
                     onClick={this.handleOpenFile.bind(this, picture.ortherStatement_C.index, picture.ortherStatement_C.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.ortherStatement_C.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.ortherStatement_C.name}
                </h4>
              </Col>
            </Row>
          </div>
          <div className={styles.carouselContainer} style={{display: this.state.showCarousel ? 'block' : 'none'}}>
            <div className={styles.carouselWrapper}>
            <Icon type="close-circle" className={styles.carouselClose} onClick={this.handleCloseCarousel}/>
            <div className={styles.carousel}>
              <Carousel {...settings} ref={(carousel) => {
                this.carousel = carousel
              }}>
              {
                Object.keys(picture).map(key => {
                  return <div key={key}><img alt={picture[key].name} src={this.presrc(picture[key].src)}/></div>
                })
              }
              </Carousel>
            </div>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <div className={styles.picture}>
            <Row type="flex" justify="space-around" align="middle" className={styles.pictureRow}>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.personal.name} src={this.presrc(picture.personal.src)}
                     onClick={this.handleOpenFile.bind(this, picture.personal.index, picture.personal.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.personal.oldValue && <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.personal.name}
                </h4>
              </Col>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.identityFront.name} src={this.presrc(picture.identityFront.src)}
                     onClick={this.handleOpenFile.bind(this, picture.identityFront.index, picture.identityFront.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.identityFront.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.identityFront.name}
                </h4>
              </Col>
            </Row>
            <Row type="flex" justify="space-around" align="middle" className={styles.pictureRow}>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.identityBack.name} src={this.presrc(picture.identityBack.src)}
                     onClick={this.handleOpenFile.bind(this, picture.identityBack.index, picture.identityBack.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.identityBack.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.identityBack.name}
                </h4>
              </Col>
              <Col span={8}>
                <img className={styles.pictureItem} alt={picture.residenceProve.name} src={this.presrc(picture.residenceProve.src)}
                     onClick={this.handleOpenFile.bind(this, picture.residenceProve.index, picture.residenceProve.src)}/>
                <h4 className={styles.pictureItemDesc}>
                  {picture.residenceProve.oldValue &&
                  <Icon className={styles.pictureOldValue} type="exclamation-circle"/>}
                  {picture.residenceProve.name}
                </h4>
              </Col>
            </Row>
          </div>
          <div className={styles.carouselContainer} style={{display: this.state.showCarousel ? 'block' : 'none'}}>
            <div className={styles.carouselWrapper}>
            <Icon type="close-circle" className={styles.carouselClose} onClick={this.handleCloseCarousel}/>
            <div className={styles.carousel}>
              <Carousel {...settings} ref={(carousel) => {
                this.carousel = carousel
              }}>
              {
                Object.keys(picture).map(key => {
                  return <div key={key}><img alt={picture[key].name} src={this.presrc(picture[key].src)}/></div>
                })
              }
              </Carousel>
            </div>
            </div>
          </div>
        </div>
      )
  }
}

export default Picture
