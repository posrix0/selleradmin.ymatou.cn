import React, {PropTypes} from 'react'
import {Popover, Icon} from 'antd'
import styles from './AuditModal.module.css'

class Title extends React.Component {
  static contextTypes = {
    status: PropTypes.number,
    isCompanySeller: PropTypes.bool
  }

  render() {
    const {status, isCompanySeller} = this.context,
      title = isCompanySeller ? '企业买手资料年审' : '个人买手资料年审',
      text = <span>{isCompanySeller ? '企业买手年审资质要求' : '个人买手年审资质要求'}</span>,
      content = isCompanySeller ? (
          <ul>
            <li><span className={styles.hightlight}>公司营业执照</span>，营业执照上的公司名、营业执照编号需与系统中提供的资料保持</li>
            <li><span className={styles.hightlight}>法人身份证明</span>，公司法人的身份证明</li>
            <li><span className={styles.hightlight}>公司银行流水对账单</span>, 需有效期在3个月内，任意一个月银行对账单，上面的公司名、地址需和系统中提交的公司名、地址保持一致
            </li>
            <li><span className={styles.hightlight}>委托授权书</span>，如账号申请人与公司法人不一致，需提供公司授权给申请人的委托授权书</li>
            <li><span className={styles.hightlight}>申请人个人近照</span>，需申请人手持身份证件拍摄，面部全部露出</li>
            <li><span className={styles.hightlight}>申请人海外身份证明</span>，需提供有效期3个月以上的海外身份证明，证件姓名与申请人（被授权人）姓名须保持一致</li>
            <li><span className={styles.hightlight}>申请人海外居住证明</span>，需提供近3个月内的居住证明，账单中的姓名需与申请人（被授权人）姓名保持一致</li>
            <li><span className={styles.hightlight}>公司名称</span>，营业执照编号：一般情况不允许更换公司名称，如需变更，必须提供通过合法渠道更名为新公司名称的相关凭证</li>
            <li><span className={styles.hightlight}>如果帐号申请人（被授权人）不为法人，且需更换，必须提供新的申请人材料和授权书。如帐号申请人为法人，则不允许更换</span></li>
          </ul>
        ) : (
          <ul>
            <li><span className={styles.hightlight}>个人近照</span>，需申请人手持身份证件拍摄，面部全部露出</li>
            <li><span className={styles.hightlight}>海外身份证明</span>，需提供有效期3个月以上的海外身份证明，证件姓名与申请人姓名须保持一致</li>
            <li><span className={styles.hightlight}>居住证明</span>，需提供近3个月内任意一个月的注册国居住证明，账单中的姓名、地址需与申请人的姓名、地址保持一致</li>
            <li><span className={styles.hightlight}>账户申请人不允许变更，否则审核拒绝</span></li>
          </ul>
        )


    const common = (
      <span>
      <span>{title}</span>
      <Popover placement="bottomLeft" title={text} content={content} trigger="hover">
        <Icon className={styles.question} type="question-circle-o"/>
      </Popover>
    </span>
    )

    switch (status) {
      case 1:
        return <div>
          { common }
          <span className={styles.status}>[待提交]</span>
        </div>
      case 2:
        return <div>
          { common }
          <span className={styles.status}>[待审核]</span>
        </div>
      case 3:
        return <div>
          { common }
          <span className={styles.status}>[退回]</span>
        </div>
      case 4:
        return <div>
          { common }
          <span className={styles.status}>[年审通过]</span>
        </div>
      case 5:
        return <div>
          { common }
          <span className={styles.status}>[拒绝待关店]</span>
        </div>
      default:
        return <div>
          <span>{title}</span>
        </div>
    }
  }
}

export default Title
