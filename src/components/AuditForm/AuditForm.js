import React, {PropTypes} from 'react'
import styles from './AuditForm.module.css'
import moment from 'moment'
import {
  Form,
  Row,
  Col,
  Input,
  Button,
  Select,
  DatePicker
} from 'antd'
import AnnualReviewMangeResource from '../../resource/AnnualReviewMangeResource'

const {RangePicker} = DatePicker
const FormItem = Form.Item
const formItemLayout = {
  labelCol: {
    span: 4
  },
  wrapperCol: {
    span: 19
  }
}
const Option = Select.Option;
const dateFormat = 'YYYY/MM/DD';

class AuditForm extends React.Component {
  static propTypes = {
    searchAudits: PropTypes.func.isRequired,
    resetQuery: PropTypes.func.isRequired,
    usernameChange: PropTypes.func.isRequired,
    emailChange: PropTypes.func.isRequired,
    phoneChange: PropTypes.func.isRequired,
    sellerTypeChange: PropTypes.func.isRequired,
    statusChange: PropTypes.func.isRequired,
    areaChange: PropTypes.func.isRequired,
    countryChange: PropTypes.func.isRequired,
    startUpdateTimeChange: PropTypes.func.isRequired,
    endUpdateTimeChange: PropTypes.func.isRequired,
    returnCountChange: PropTypes.func.isRequired,
    oldNameChange: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      continents: []
    }
  }

  componentDidMount() {
    AnnualReviewMangeResource.getAllContinentInfo().then(response => {
      this.setState({
        continents: response.data.Data
      })
    })
  }

  handleInputChange = (...args) => {
    const [action, e] = args,
      {value} = e.target
    action(value)
  }

  handleSelectChange = (...args) => {
    const [action, value] = args
    action(value)
  }

  handleUpdateTimeChange = (moment, date) => {
    const [start, end] = date,
    {startUpdateTimeChange, endUpdateTimeChange} = this.props

    startUpdateTimeChange(start)
    endUpdateTimeChange(end)
  }

  onSearch = () => {
    this.props.searchAudits()
  }

  onReset = () => {
    this.props.form.resetFields()
    this.props.resetQuery()
  }

  render() {
    const {getFieldDecorator} = this.props.form

    return (
      <Form className={styles.wrapper}>
        <Row gutter={16}>
          <Col span={6}>
            <FormItem {...formItemLayout} label="用户名">
              {getFieldDecorator('username')(<Input
                onChange={this.handleInputChange.bind(this, this.props.usernameChange)}/>)}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem {...formItemLayout} label="邮箱">
              {getFieldDecorator('email')(<Input
                onChange={this.handleInputChange.bind(this, this.props.emailChange)}/>)}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem {...formItemLayout} label="手机号">
              {getFieldDecorator('telephone')(<Input placeholder="请添加国际区号"
                                                     onChange={this.handleInputChange.bind(this, this.props.phoneChange)}/>)}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem {...formItemLayout} label="买手类型">
              {getFieldDecorator('type')(
                <Select placeholder="全部" initialValue="null"
                        onChange={this.handleSelectChange.bind(this, this.props.sellerTypeChange)}>
                  <Option value="null">全部</Option>
                  <Option value="false">个人买手</Option>
                  <Option value="true">企业买手</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={6}>
            <FormItem {...formItemLayout} label="状态">
              {getFieldDecorator('status')(
                <Select placeholder="全部" onChange={this.handleSelectChange.bind(this, this.props.statusChange)}>
                  <Option value="0">全部</Option>
                  <Option value="1">年审待提交</Option>
                  <Option value="2">年审待审核</Option>
                  <Option value="3">年审退回</Option>
                  <Option value="5">年审拒绝待关闭</Option>
                  <Option value="4">年审通过</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem {...formItemLayout} label="选择大区">
              {getFieldDecorator('area')(
                <Select placeholder="全部" initialValue="" onChange={this.handleSelectChange.bind(this, this.props.areaChange)}>
                  <Option value="">全部</Option>
                  {
                    this.state.continents.map((continent, index) => {
                      return <Option key={index} value={continent.ContinentName}>{continent.ContinentName}</Option>
                    })
                  }
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem {...formItemLayout} label="选择国家">
              {getFieldDecorator('country')(
                <Select placeholder="全部" initialValue="" onChange={this.handleSelectChange.bind(this, this.props.countryChange)}>
                  <Option value="">全部</Option>
                  {
                    this.state.continents.map(continent => {
                      return continent.CountryList.map((country, index) => {
                        return <Option key={index} title={country.CountryNameZh} value={country.CountryId.toString()}>{country.CountryNameZh}</Option>
                      })
                    })
                  }
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem {...formItemLayout} label="提交时间">
              {getFieldDecorator('submitTime')(<RangePicker initialValue={[
                moment('2015/01/01', dateFormat),
                moment('2015/01/01', dateFormat)
              ]} onChange={this.handleUpdateTimeChange} format={dateFormat}/>)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={6}>
            <FormItem {...formItemLayout} label="提交次数">
              {getFieldDecorator('returnCount')(
                <Select placeholder="全部" initialValue="" onChange={this.handleSelectChange.bind(this, this.props.returnCountChange)}>
                  <Option value="">全部</Option>
                  <Option value="0">第一次提交</Option>
                  <Option value="1">第二次提交</Option>
                  <Option value="2">第三次提交</Option>
                  <Option value="3">三次以上</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem {...formItemLayout} label="曾用名">
              {getFieldDecorator('oldName')(<Input
                onChange={this.handleInputChange.bind(this, this.props.oldNameChange)}/>)}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Button type="primary" htmlType="submit" size="large" onClick={this.onSearch}>搜索</Button>
            <Button className={styles.resetButton} htmlType="submit" size="large" onClick={this.onReset}>清空</Button>
          </Col>
        </Row>
      </Form>
    )
  }
}

export default Form.create()(AuditForm)
