import React from 'react'
import {Link} from 'react-router'
import {Menu, Icon} from 'antd'
import axios from 'axios'
import './layout.css'
import 'antd/lib/menu/style/css'
import 'antd/lib/icon/style/css'

const SubMenu = Menu.SubMenu,
 MenuItemGroup = Menu.ItemGroup

function getCookie(key) {
  for (var cookies = document.cookie.split("; "), i = 0; i < cookies.length; i++) {
    var item = cookies[i].split("=");
    if (key === item[0]) {
      try {
        return decodeURIComponent(item[1])
      } catch (e) {
        return null
      }
    }
  }
  return null
}

const env = process.env.NODE_ENV,
  ROOT = env === 'development' ? '/admin/' : `http://a.ymatou.cn/admin/`

const linkUrl = {
  'CurrentUserMenus': `${ROOT}PageMenu/CurrentUserMenus`,
  'GetAllUserInfoByToken': `${ROOT}UserManager/GetAllUserInfoByToken`
}

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //菜单
      menus: [],
      curUrl: '',
      current: '84',
      openKeys: [''],
      //用户
      loading: false,
      user: {
        Name: '--',
      },
      auth: {
        success: true,
        msg: '',
      }
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleOpenChange = this.handleOpenChange.bind(this);
    this.fetchMeuns = this.fetchMeuns.bind(this);

    this.checkUser().then(this.fetchMeuns);

    //跨系统链接，指定默认选中
    // const _cur = /_cur=([\d,]+)/.exec(location.search);
    // this.cur = _cur ? _cur[1].split(',') : ['373', '84'];
  }

  componentDidMount() {
    this.refs.sider.style.minHeight = document.body.offsetHeight + 'px';
    window.onresize = () => {
      if (this.refs.sider) {
        this.refs.sider.style.minHeight = document.body.offsetHeight + 'px';
      }
    }
  }

  /**
   * 验证用户信息
   */
  checkUser() {
    const token = this.token = getCookie('ManageToken');
    if (!token) {
      document.location.href = `http://admin.ymatou.cn/login?ret=${encodeURIComponent(document.location.href)}`;
      return;
    }
    return axios.get(linkUrl.GetAllUserInfoByToken, { params: { token } }).then((result) => {
      if (result.status === 200 && result.data) {
        this.setState({ user: result.data });
        return Promise.resolve(result.data);
      } else {
        this.setState({
          auth: {
            success: false,
            msg: '当前用户没有登录或没有权限'
          }
        });
      }
    }, (error) => {
      this.setState({
        auth: {
          success: false,
          msg: String(error),
        }
      });
    });
  }

  /**
   * 获取菜单列表
   */
  fetchMeuns() {
    axios.get(linkUrl.CurrentUserMenus).then(result => {
      if (result.status === 200 && result.data.Success) {
        const currentArr = this.fundCurrentMenus(result.data.Data);
        if (currentArr) {
          this.setState({
            menus: result.data.Data || [],
            current: currentArr[2],
            openKeys: [currentArr[0]],
          });
        } else {
          this.setState({
            menus: result.data.Data || [],
          });
        }
      }
    });
  }

  /**
   * 找到当前菜单路径
   * @param  {Array} menus 菜单数组
   * @return {Array} currentArr 当前路径数组
   */
  fundCurrentMenus(menus) {
    let currentArr;
    (function fn(items, _arr) {
      for (let i = 0; i < items.length; i++) {
        let arr = _arr || [];
        let item = items[i];
        if (item.Children && item.Children.length > 0) {
          fn(item.Children, arr.concat([item.Id.toString()]));
        } else {
          //高亮当前页面
          if (item.Url && location.href.indexOf(item.Url) >= 0) {
            currentArr = arr.concat([item.Id.toString()]);
            return;
          }
        }
      }
    })(menus || []);
    return currentArr;
  }

  handleClick(e) {
    this.setState({ current: e.key, openKeys: e.keyPath.slice(1) });
  }

  handleOpenChange(openKeys) {
    this.setState({ openKeys });
  }

  render() {
    const iconMap = {
      'service': 'customerservice',
      'Auth': 'tag',
      'finance': 'pay-circle-o',
      'seller': 'user',
      'marketing': 'appstore-o',
      'CRM': 'bar-chart',
      'products': 'shopping-cart',
      'community': 'team',
      'operation': 'solution'
    }
    const items = this.state.menus;
    const urlDomainReg = /\/(\w+)/;
    const linkDomainReg = /https?:\/\/a\.ymatou\.cn\/(\w+)/;
    return (
      <div className="ant-layout-aside">
        <aside className="ant-layout-sider" ref="sider">
          <div className="ant-layout-logo"></div>
          <Menu mode="inline" theme="dark"
                onClick={ this.handleClick }
                onOpenChange={ this.handleOpenChange }
                openKeys={ this.state.openKeys }
                selectedKeys={[this.state.current]} >
            {
              items.map((item) => {
                return (
                  <SubMenu key={ item.Id } title={ <span><Icon type={ iconMap[item.Name] } /> <span className="sub-name">{ item.Text } </span></span> }>
                    {
                      item.Children.map((node) => {
                        return (
                          <MenuItemGroup key={ node.Id } title={ node.Text }>
                            {
                              node.Children.map((page) => {
                                let anchor = '';
                                const isNew = page.Url.indexOf('http://a.ymatou.cn') >= 0;
                                if (isNew) {
                                  const urlDomain = urlDomainReg.exec(location.pathname) || [];
                                  const linkDomain = linkDomainReg.exec(page.Url) || [];

                                  if(urlDomain && urlDomain[1] === linkDomain[1]){
                                    //如果二级域名相同，则用router跳转
                                    anchor = (<Link to={ page.Url.replace(linkDomain[0], '') }>{ page.Text }</Link>)
                                  }else{
                                    anchor = (<a href={ page.Url }>{ page.Text }</a>)
                                  }
                                } else {
                                  anchor = (<a href={`http://admin.ymatou.cn/Menu#!${page.Id}!`}>{page.Text}</a>)
                                }
                                return (
                                  <Menu.Item key={ page.Id }>{ anchor }</Menu.Item>
                                )
                              })
                            }
                          </MenuItemGroup>
                        )
                      })
                    }
                  </SubMenu>
                )
              })
            }
          </Menu>
        </aside>
        <div className="ant-layout-main">
          <div className="ant-layout-header">
            <div className="user-info" >
              <span>欢迎进入洋码头客服系统  { this.state.user && this.state.user.Name }</span>
              <span><a href="http://admin.ymatou.cn/Account/MyProfile"><Icon type="setting" /> 设置</a></span>
              <span><a href="http://admin.ymatou.cn/Login/LogOut"><Icon type="logout" /> 退出</a></span>
            </div>
          </div>
          <div className="ant-layout-container">
            <div className="ant-layout-content">
              <div>
                {
                  this.state.auth.success
                  ? this.props.children
                  : (
                      <div className="error-area">
                        <div className="error-alert">
                          <span className="error-alert-message">没有权限</span>
                          <span className="error-alert-description">{ this.state.auth.msg }</span>
                        </div>
                        <a className="error-btn" href={`http://admin.ymatou.cn/login?ret=${encodeURIComponent(document.location.href)}`}><span>返回登录</span></a>
                      </div>
                    )
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

module.exports = Main;
