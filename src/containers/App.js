import React from 'react'
import AuditContainer from './AuditContainer'
import AuditFilterContainer from './AuditFilterContainer'
import '../assets/global.css'
import 'nprogress/nprogress.css'

const App = () => (
  <div>
    <AuditFilterContainer/>
    <AuditContainer/>
  </div>
)

export default App
