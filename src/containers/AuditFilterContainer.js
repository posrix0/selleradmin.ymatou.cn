import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import AuditForm from '../components/AuditForm/AuditForm'
import {
  searchAudits,
  resetQuery,
  usernameChange,
  emailChange,
  phoneChange,
  sellerTypeChange,
  statusChange,
  areaChange,
  startUpdateTimeChange,
  endUpdateTimeChange,
  countryChange,
  returnCountChange,
  oldNameChange
} from '../actions'

const AuditFilterContainer = ({
  searchAudits,
  resetQuery,
  usernameChange,
  emailChange,
  phoneChange,
  sellerTypeChange,
  statusChange,
  areaChange,
  startUpdateTimeChange,
  endUpdateTimeChange,
  countryChange,
  returnCountChange,
  oldNameChange
}) => (<AuditForm
  searchAudits={searchAudits}
  resetQuery={resetQuery}
  usernameChange={usernameChange}
  emailChange={emailChange}
  phoneChange={phoneChange}
  sellerTypeChange={sellerTypeChange}
  statusChange={statusChange}
  areaChange={areaChange}
  startUpdateTimeChange={startUpdateTimeChange}
  endUpdateTimeChange={endUpdateTimeChange}
  countryChange={countryChange}
  returnCountChange={returnCountChange}
  oldNameChange={oldNameChange}/>)

AuditFilterContainer.propTypes = {
  searchAudits: PropTypes.func.isRequired,
  resetQuery: PropTypes.func.isRequired,
  usernameChange: PropTypes.func.isRequired,
  emailChange: PropTypes.func.isRequired,
  phoneChange: PropTypes.func.isRequired,
  sellerTypeChange: PropTypes.func.isRequired,
  statusChange: PropTypes.func.isRequired,
  areaChange: PropTypes.func.isRequired,
  startUpdateTimeChange: PropTypes.func.isRequired,
  endUpdateTimeChange: PropTypes.func.isRequired,
  countryChange: PropTypes.func.isRequired,
  returnCountChange: PropTypes.func.isRequired,
  oldNameChange: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, {
  searchAudits,
  resetQuery,
  usernameChange,
  emailChange,
  phoneChange,
  sellerTypeChange,
  statusChange,
  startUpdateTimeChange,
  endUpdateTimeChange,
  areaChange,
  countryChange,
  returnCountChange,
  oldNameChange
})(AuditFilterContainer)
