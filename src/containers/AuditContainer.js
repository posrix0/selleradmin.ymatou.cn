import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import AuditTable from '../components/AuditTable/AuditTable'
import AuditModal from '../components/AuditModal/AuditModal'
import {tabStatusChange, pageChange, toggleAuditModalVisible, generateAuditModalRandomKey, modifyAuditStatus, getAuditDetail} from '../actions'

const AuditContainer = ({
  collection,
  query,
  total,
  pageChange,
  tabStatusChange,
  toggleAuditModalVisible,
  generateAuditModalRandomKey,
  modifyAuditStatus,
  auditModalVisible,
  auditModalRandomKey,
  getAuditDetail,
  annualReview,
  annualReviewCompanyInfo,
  annualReviewUserInfo,
  annualReviewLogList
}) => (
  <div>
    <AuditTable
      collection={collection}
      query={query}
      tabStatusChange={tabStatusChange}
      total={total}
      pageChange={pageChange}
      toggleAuditModalVisible={toggleAuditModalVisible}
      getAuditDetail={getAuditDetail}/>
    <AuditModal
      modifyAuditStatus={modifyAuditStatus}
      auditModalVisible={auditModalVisible}
      auditModalRandomKey={auditModalRandomKey}
      generateAuditModalRandomKey={generateAuditModalRandomKey}
      toggleAuditModalVisible={toggleAuditModalVisible}
      annualReview={annualReview}
      annualReviewCompanyInfo={annualReviewCompanyInfo}
      annualReviewUserInfo={annualReviewUserInfo}
      annualReviewLogList={annualReviewLogList}/>
  </div>
)

AuditContainer.propTypes = {
  collection: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
  })).isRequired,
  query: PropTypes.object.isRequired,
  total: PropTypes.number.isRequired,
  pageChange: PropTypes.func.isRequired,
  tabStatusChange: PropTypes.func.isRequired,
  toggleAuditModalVisible: PropTypes.func.isRequired,
  generateAuditModalRandomKey: PropTypes.func.isRequired,
  getAuditDetail: PropTypes.func.isRequired,
  auditModalVisible: PropTypes.bool.isRequired,
  auditModalRandomKey: PropTypes.number.isRequired,
  modifyAuditStatus: PropTypes.func.isRequired,
  annualReview: PropTypes.object,
  annualReviewUserInfo: PropTypes.object,
  annualReviewCompanyInfo: PropTypes.object,
  annualReviewLogList: PropTypes.array
}

const mapStateToProps = (state) => ({
  collection: state.audit.page.collection,
  query: state.filter.query,
  total: state.audit.page.total,
  auditModalVisible: state.audit.item.visible,
  auditModalRandomKey: state.audit.item.randomKey,
  annualReview: state.audit.item.annualReview,
  annualReviewUserInfo: state.audit.item.annualReviewUserInfo,
  annualReviewCompanyInfo: state.audit.item.annualReviewCompanyInfo,
  annualReviewLogList: state.audit.item.annualReviewLogList
})

export default connect(mapStateToProps, {
  pageChange,
  tabStatusChange,
  toggleAuditModalVisible,
  generateAuditModalRandomKey,
  modifyAuditStatus,
  getAuditDetail
 })(AuditContainer)
