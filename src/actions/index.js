import AnnualReviewMangeResource from '../resource/AnnualReviewMangeResource'
import * as types from '../constants/ActionTypes'
import { INIT_QUERY } from '../constants/Query'

const getAuditsByPage = (query, cb) => (dispatch, getState) => {
  query = !query ?
    getState().filter.query :
    query

  AnnualReviewMangeResource.getAuditsByPage(query)
    .then(response => {
      const Data = response.data.Data
      const { Rows, Total } = Data
      dispatch({
        type: types.RECEIVE_AUDITS_BY_PAGE,
        rawAuditRows: Rows
      })
      dispatch({
        type: types.RECEIVE_AUDITS_TOTAL,
        total: Total
      })
      cb && cb({ ...Data })
    })
}

export const getAuditById = (auditId, cb) => dispatch => {
  AnnualReviewMangeResource.getAuditById(auditId)
    .then(response => {
      const { Data } = response.data
      dispatch({
        type: types.RECEIVE_AUDIT_BY_ID,
        rawAudit: {
          ...Data
        }
      })
      cb && cb({ ...Data })
    })
}

export const getAllAudits = (cb) => dispatch => {
  dispatch(getAuditsByPage(INIT_QUERY, cb))
}

export const getAuditDetail = (auditId, cb) => dispatch => {
  dispatch(getAuditById(auditId, cb))
}

export const toggleAuditModalVisible = (visible) => dispatch => {
  dispatch({
    type: types.AUDIT_MODAL_VISIBLE,
    visible
  })
}

export const generateAuditModalRandomKey = () => dispatch => {
  dispatch({
    type: types.AUDIT_MODAL_RANDOM_KEY,
    randomKey: Math.floor(Math.random() * 10000)
  })
}

const pageChangeCreator = page => ({
  type: types.PAGE_CHANGE,
  page
})

export const statusChange = status => dispatch => {
  dispatch({
    type: types.STATUS_CHANGE,
    status
  })
}

export const tabStatusChange = tabStatus => dispatch => {
  dispatch(pageChangeCreator(1))
  dispatch({
    type: types.TAB_STATUS_CHANGE,
    tabStatus
  })
  dispatch(getAuditsByPage())
}

export const pageChange = page => dispatch => {
  dispatch(pageChangeCreator(page))
  dispatch(getAuditsByPage())
}

export const searchAudits = () => dispatch => {
  dispatch(pageChangeCreator(1))
  dispatch(getAuditsByPage())
}

export const resetQuery = () => dispatch => {
  dispatch({
    type: types.RESET_QUERY
  })
  dispatch(getAuditsByPage())
}

export const usernameChange = username => dispatch => {
  dispatch({
    type: types.USERNAME_CHANGE,
    username
  })
}

export const emailChange = email => dispatch => {
  dispatch({
    type: types.EMAIL_CHANGE,
    email
  })
}

export const phoneChange = phone => dispatch => {
  dispatch({
    type: types.PHONE_CHANGE,
    phone
  })
}

export const sellerTypeChange = sellerType => dispatch => {
  dispatch({
    type: types.SELLER_TYPE_CHANGE,
    sellerType
  })
}

export const areaChange = area => dispatch => {
  dispatch({
    type: types.AREA_CHANGE,
    area
  })
}

export const startUpdateTimeChange = startUpdateTime => dispatch => {
  dispatch({
    type: types.START_UPDATE_TIME_CHANGE,
    startUpdateTime
  })
}

export const endUpdateTimeChange = endUpdateTime => dispatch => {
  dispatch({
    type: types.END_UPDATE_TIME_CHANGE,
    endUpdateTime
  })
}

export const countryChange = country => dispatch => {
  dispatch({
    type: types.COUNTRY_CHANGE,
    country
  })
}

export const returnCountChange = returnCount => dispatch => {
  dispatch({
    type: types.RETURN_COUNT_CHANGE,
    returnCount
  })
}

export const oldNameChange = oldName => dispatch => {
  dispatch({
    type: types.OLD_NAME_CHANGE,
    oldName
  })
}

export const modifyAuditStatus = (statusText, auditId) => (dispatch, getState) => {
  dispatch({
    type: types.MODIFY_AUDIT_STATUS,
    collection: getState().audit.page.collection,
    statusText,
    auditId
  })
}
