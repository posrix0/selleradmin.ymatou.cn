import { combineReducers } from 'redux'
import * as types from '../constants/ActionTypes'

const initState = {
  page: {
    collection: [],
    total: 0
  },
  item: {
    annualReview: {},
    annualReviewUserInfo: {},
    annualReviewCompanyInfo: {},
    annualReviewLogList: [],
    visible: false,
    randomKey: 0
  }
}

const page = (state = initState.page, action) => {
  switch(action.type) {
    case types.RECEIVE_AUDITS_BY_PAGE:
      return {
        ...state,
        collection: action.rawAuditRows.map(rawAuditRow => ({
          key: rawAuditRow.AnnualReviewId,
          date: rawAuditRow.SubmitTime,
          username: rawAuditRow.SellerLoginName,
          country: rawAuditRow.CountryName,
          email: rawAuditRow.Email,
          status: rawAuditRow.StatusText,
          type: rawAuditRow.IsCompanySeller ? '企业买手' : '个人买手',
          operation: '进入'
        }))
      }
    case types.MODIFY_AUDIT_STATUS:
      return {
        ...state,
        collection: action.collection.map(audit => {
          if (audit.key === action.auditId) {
            return {
              ...audit,
              status: action.statusText
            }
          } else {
            return {
              ...audit
            }
          }
        })
      }
    case types.RECEIVE_AUDITS_TOTAL:
      return {
        ...state,
        total: action.total
      }
    default:
      return state
  }
}

const item = (state = initState.item, action) => {
  switch(action.type) {
    case types.RECEIVE_AUDIT_BY_ID:
      return {
        ...state,
        annualReview: action.rawAudit.AnnualReview || {},
        annualReviewUserInfo: action.rawAudit.AnnualReviewUserInfo || {},
        annualReviewCompanyInfo: action.rawAudit.AnnualReviewCompanyInfo || {},
        annualReviewLogList: action.rawAudit.AnnualReviewLogList || []
      }
    case types.AUDIT_MODAL_VISIBLE:
      return {
        ...state,
        visible: action.visible
      }
    case types.AUDIT_MODAL_RANDOM_KEY:
      return {
        ...state,
        randomKey: action.randomKey
      }
    default:
      return state
  }
}

export default combineReducers({
  page,
  item
})
