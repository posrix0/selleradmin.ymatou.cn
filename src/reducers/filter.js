import { combineReducers } from 'redux'
import * as types from '../constants/ActionTypes'
import { INIT_QUERY } from '../constants/Query'

const query = (state = INIT_QUERY, action) => {
  switch(action.type) {
    case types.PAGE_CHANGE:
      return {
        ...state,
        PageIndex: action.page
      }
    case types.STATUS_CHANGE:
      return {
        ...state,
        Status: action.status
      }
    case types.TAB_STATUS_CHANGE:
      return {
        ...state,
        TabStatus: action.tabStatus
      }
    case types.USERNAME_CHANGE:
      return {
        ...state,
        Name: action.username
      }
    case types.EMAIL_CHANGE:
      return {
        ...state,
        Email: action.email
      }
    case types.PHONE_CHANGE:
      return {
        ...state,
        Phone: action.phone
      }
    case types.SELLER_TYPE_CHANGE:
      return {
        ...state,
        IsCompanySeller: action.sellerType
      }
    case types.COUNTRY_CHANGE:
      return {
        ...state,
        Country: action.country
      }
    case types.AREA_CHANGE:
      return {
        ...state,
        Area: action.area
      }
    case types.START_UPDATE_TIME_CHANGE:
      return {
        ...state,
        StartUpdateTime: action.startUpdateTime
      }
    case types.END_UPDATE_TIME_CHANGE:
      return {
        ...state,
        EndUpdateTime: action.endUpdateTime
      }
    case types.RETURN_COUNT_CHANGE:
      return {
        ...state,
        ReturnCount: action.returnCount
      }
    case types.OLD_NAME_CHANGE:
      return {
        ...state,
        OldName: action.oldName
      }
    case types.RESET_QUERY:
      return {
        ...INIT_QUERY
      }
    default:
      return state
  }
}

export default combineReducers({
  query
})
