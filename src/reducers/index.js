import { combineReducers } from 'redux'
import audit from './audit'
import filter from './filter'

export default combineReducers({
  audit,
  filter
})
