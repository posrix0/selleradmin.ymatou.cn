export default {
  WAITING_POST: {
    ID: 1,
    TEXT: '待提交'
  },
  WAITING_AUDIT: {
    ID: 2,
    TEXT: '待审核'
  },
  REJECT: {
    ID: 3,
    TEXT: '退回'
  },
  APPROVE: {
    ID: 4,
    TEXT: '年审通过'
  },
  REJECT_FOREVER: {
    ID: 5,
    TEXT: '拒绝待关店'
  }
}
