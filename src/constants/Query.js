export const INIT_QUERY = {
  Name: '',
  Email: '',
  Phone: '',
  Status: 0,
  TabStatus: 0,
  IsCompanySeller: null,
  Area: '',
  Country: '',
  ReturnCount: null,
  OldName: '',
  StartUpdateTime: '',
  EndUpdateTime: '',
  PageIndex: 1,
  PageSize: 10
}
