import {
  AnnualReviewResource
} from './'

export default {
  auditApprove: (body) =>
    AnnualReviewResource.post('/AuditPassed', {
      ...body
    }),
  auditReject: (body) =>
    AnnualReviewResource.post('/AuditToback', {
      ...body
    }),
  auditRejectForever: (body) =>
    AnnualReviewResource.post('/AuditReject', {
      ...body
    }),
  auditRejectRevoke: (body) =>
    AnnualReviewResource.post('/AuditRevoke', {
      ...body
    })
}
