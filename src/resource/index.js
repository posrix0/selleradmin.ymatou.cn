import axios from 'axios'
import nprogress from 'nprogress'
import { Modal } from 'antd'
import prodConfig from '../../config/prod.js'

axios.defaults.timeout = 180000

const beforeRequest = [
  config => {
    nprogress.start()

    return config
  },
  error => {
    nprogress.done()

    Modal.error({
      title: error,
      content: error.toString()
    })
    return Promise.reject(error)
  }
]

const afterResponse = [
  response => {
    nprogress.done()

    if (response.data.NoPermission) {
      const error = '您暂无权限执行此操作，请联系管理员确认！'
      Modal.error({
        title: error
      })
      return Promise.reject(error)
    } else if (response.data.Code !== 200) {
      const error = response.data.Msg
      Modal.error({
        title: error
      })
      return Promise.reject(error)
    } else if (!response.data.Success) {
      const error = 'Network Error'
      Modal.error({
        title: error
      })
      return Promise.reject(error)
    } else {
      return response
    }
  },
  error => {
    nprogress.done()

    Modal.error({
      title: 'Response Error',
      content: error.toString()
    })
    return Promise.reject(error)
  }
]

const create = axios.create,
  env = process.env.NODE_ENV,
  ROOT = env === 'development' ? '/api/' : `${prodConfig.csHost}/api/`,
  resources = {
    AnnualReviewMangeResource: create({
      baseURL: `${ROOT}AnnualReview/AnnualReviewMange`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    }),
    AnnualReviewResource: create({
      baseURL: `${ROOT}AnnualReview/Approve`,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
  }

for (let key of Object.keys(resources)) {
  resources[key].interceptors.request.use(...beforeRequest)
  resources[key].interceptors.response.use(...afterResponse)
}

export { ROOT }
export const { AnnualReviewMangeResource, AnnualReviewResource } = resources
