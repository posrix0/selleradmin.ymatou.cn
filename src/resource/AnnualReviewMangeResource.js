import {
  AnnualReviewMangeResource
} from './'

export default {
  getAuditsByPage: (body) =>
    AnnualReviewMangeResource.post('/SearchAnnualReview', {
      ...body
    }),
  getAuditById: (auditId) =>
    AnnualReviewMangeResource.get('/GetAnnualReviewById', {
      params: {
        annualReviewId: auditId
      }
    }),
  getRejectReasonTypeList: (isCompanySeller) =>
    AnnualReviewMangeResource.get('/GetRejectReasonTypeList', {
      params: {
        IsCompanySeller: isCompanySeller
      }
    }),
  getAllContinentInfo: () =>
    AnnualReviewMangeResource.get('/GetAllContinentInfo'),
  getFeedbacksByLogId: (logId) =>
    AnnualReviewMangeResource.get('/GetFeedbacksByLogId', {
      params: {
        logId
      }
    })
}
