import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import createLogger from 'redux-logger'
import { Router, Route, useRouterHistory, IndexRedirect } from 'react-router'
import { createHistory } from 'history'
import thunk from 'redux-thunk'
import layout from './components/layout'
import reducer from './reducers'
import { getAllAudits } from './actions'
import App from './containers/App'

const middleware = [ thunk ]

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger())
}

const store = createStore(
  reducer,
  applyMiddleware(...middleware)
)

store.dispatch(getAllAudits())

const routeHistory = useRouterHistory(createHistory)({
  basename: '/selleradmin'
})

render(
  <Provider store={store}>
    <Router history={routeHistory}>
      <Route path="/" component={layout}>
        <IndexRedirect to="/audit" />
        <Route path="/audit" component={App} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
